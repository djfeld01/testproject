import React from 'react';
import logo from './logo.svg';
import './App.scss';
import { Menu } from './menu/menu';


function App() {
  return (
    <div className="App">
      <div className="left-menu">
         <Menu />
      </div>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
        
        </a>
      </header>
    </div>
  );
}

export default App;


